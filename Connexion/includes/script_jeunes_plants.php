<?php

date_default_timezone_set('Europe/Paris');
// Connexion à la base de données MySQL

include "includes/database.php";


if (isset($_POST["export"])) {
	// Set headers to force download
	header('Content-Type: text/csv');
	header('Content-Disposition: attachment; filename="données_hygrométrique.csv"');

	// Open a file pointer connected to output stream
	$output = fopen('php://output', 'w');

	// Write the column headers to CSV
	fputcsv($output, array('id_mesure', 'heure_mesure', 'temperature', 'hygrometrie'));

	// Connect to database and select data
	$stmt = $pdo->prepare('SELECT id_mesure, heure_mesure, temperature, hygrometrie, FROM mesure');
	$stmt->execute();

	// Write data rows to CSV
	while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
		fputcsv($output, $row);
	}

	// Close file pointer
	fclose($output);
	exit();
}



/*
$nb_points = 10;
if(isset($_GET['ok']))
{
	$nb_points = $_GET['nb_points'];
}
// Requête SQL pour récupérer la donnée numérique dans la colonne mesure
$sql = "SELECT * FROM mesure WHERE capteur = 'capteur_hygro' ORDER BY heure_mesure DESC LIMIT $nb_points; ";
$result = mysqli_query($conn, $sql);

// Création du tableau de données pour le graphique et des variables 
$data = array();
$time = array(); 
$data_hygro = 0;
$data_temp = 0;
$row = mysqli_fetch_assoc($result);

$data_hygro = $row["hygrometrie"];
$data_temp = $row["temperature"];

while ($row = mysqli_fetch_assoc($result)){
	$data[] = $row["hygrometrie"];
	$time[] = date('d M, H\hi',strtotime($row["heure_mesure"]));	
} 
$data = array_reverse($data);
$time = array_reverse($time);
// Fermeture de la connexion à la base de données MySQL
mysqli_close($conn);


*/
