<?PHP
//Envoi les donnees sur la carte

// Connexion à la base de données MySQL
$host = 'localhost';
$user = 'root';
$pass = '';
$dbname = 'obs_mesure';
$conn = mysqli_connect($host, $user, $pass, $dbname);

// Vérification de la connexion
if (!$conn) {
    die("La connexion à la base de données a échoué : " . mysqli_connect_error());
}

// Requête SQL pour récupérer la donnée numérique
$sql = "SELECT * FROM control_data ORDER BY id_control_data DESC LIMIT 1; ";
$result = mysqli_query($conn, $sql);

$row = mysqli_fetch_assoc($result);

$seuil = $row["seuil"];
$duree = $row["duree"];

$duree_ms = $duree*1000; 
echo "$seuil,$duree_ms"; 

// Fermeture de la connexion à la base de données MySQL
mysqli_close($conn);
?>