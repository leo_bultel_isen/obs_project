<?php
date_default_timezone_set('Europe/Paris');
// Connexion à la base de données MySQL
$host = 'localhost';
$user = 'root';
$pass = '';
$dbname = 'loginsystem';
$conn = mysqli_connect($host, $user, $pass, $dbname);
// Vérification de la connexion
if (!$conn) {
    die("La connexion à la base de données a échoué : " . mysqli_connect_error());
}
$nb_points = 10;
if(isset($_GET['ok']))
{
	$nb_points = $_GET['nb_points'];
}
// Requête SQL pour récupérer la donnée numérique dans la colonne mesure
$sql = "SELECT * FROM mesure WHERE capteur = 'capteur_humi' ORDER BY heure_mesure DESC LIMIT $nb_points; ";
$result = mysqli_query($conn, $sql);

// Création du tableau de données pour le graphique et des variables 
$data = array();
$time = array(); 
$data_humi = 0;
$row = mysqli_fetch_assoc($result);
$data_humi = $row["humidite"];

while ($row = mysqli_fetch_assoc($result)){
	$data[] = $row["humidite"];
	$time[] = date('d M, H\hi',strtotime($row["heure_mesure"]));	
}
$data = array_reverse($data);
$time = array_reverse($time);
// Fermeture de la connexion à la base de données MySQL
mysqli_close($conn);
