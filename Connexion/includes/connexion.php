<?php

include 'database.php';

if(isset($_POST['submit'])){

    // Récupérer les données du formulaire
    $email = $_POST['email'];
    $password = $_POST['pass'];

    // Requête SQL pour récupérer l'utilisateur correspondant à l'email
    $stmt = $conn->prepare("SELECT * FROM user WHERE email=:email");
    $stmt->bindParam(':email', $email);
    $stmt->execute();

    // Vérifier si l'utilisateur existe dans la base de données
    if($stmt->rowCount() == 1){

        // Récupérer les données de l'utilisateur
        $user = $stmt->fetch();

        // Vérifier si le mot de passe est correct en utilisant la fonction password_verify()
        if(password_verify($password, $user['password'])){

            // Démarrer la session
            session_start();

            // Stocker les informations de l'utilisateur dans la session
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['user_email'] = $user['email'];

            // Rediriger l'utilisateur vers une page sécurisée
            header("Location: ../accueil");
            exit();

        } else {  ?>


            <!DOCTYPE html>
            <html lang="fr">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link rel="stylesheet" href="../css/erreur_redirection.css">
                <link rel="icon" type="image/png" href="../images/628283.png">
                <link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">

                <title>Erreur mot de passe</title>
            </head>
            <body>
            <div id="notfound">
		<div class="notfound">
			<div class="notfound-404"></div>
			<h1>Erreur</h1>
			<h2>Une erreur est survenue...Il semble que le mot de passe ne soit pas correct</h2>
			<p>Veuillez rentrer mot de passe correct</p>
			<a href="../index.php">Retour à la page de connexion</a>
		</div>
	</div>  
            </body>
            </html>

    <?php
            // Afficher un message d'erreur si le mot de passe est incorrect
        }

    } else {
        ?>

<!DOCTYPE html>
            <html lang="fr">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <link rel="stylesheet" href="../css/erreur_redirection.css">
                <link rel="icon" type="image/png" href="../images/628283.png">
                <link href="https://fonts.googleapis.com/css?family=Nunito:400,700" rel="stylesheet">

                <title>Erreur mot de passe</title>
            </head>
            <body>
            <div id="notfound">
		<div class="notfound">
			<div class="notfound-404"></div>
			<h1>Erreur</h1>
			<h2>Une erreur est survenue...Il semble que l'utilisateur n'existe pas</h2>
			<p>Veuillez rentrer une adresse email valide</p>
			<a href="../index.php">Retour à la page de connexion</a>
		</div>
	</div>
            </body>
            </html>
        <?php
    }

    // Fermeture de la connexion à la base de données
    $dbh = null;
}
?>