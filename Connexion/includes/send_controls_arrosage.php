<?PHP

// Connexion à la base de données MySQL
$host = 'localhost';
$user = 'root';
$pass = '';
$dbname = 'obs_mesure';
$conn = mysqli_connect($host, $user, $pass, $dbname);

// Vérification de la connexion
try {
  $conn = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
  // définir le mode exception d'erreur PDO 
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
  $sql = "INSERT INTO `control_data` ( `duree`, `seuil`) VALUES ( '$_POST[duree_arrosage]','$_POST[taux_humi_arrosage]')";
  // utiliser la fonction exec() car aucun résultat n'est renvoyé
  $conn->exec($sql);
} catch(PDOException $e) {
  echo $sql . "<br>" . $e->getMessage();
}
$conn = null;
?>