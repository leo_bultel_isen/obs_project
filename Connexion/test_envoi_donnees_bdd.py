import mysql.connector
from datetime import datetime
import random

try:
    connection = mysql.connector.connect(
        host="localhost",
        user="root",
        password="",
        database="loginsystem"
      )
    print("Connexion à la BDD réussie")
    mySql_insert = """INSERT INTO mesure (heure_mesure,hygrometrie,capteur,temperature,humidite) VALUES (%s,%s,%s,%s,%s) """

    chrono = str(datetime.now()) # exemple de valeur pour le champ chrono
    hygrometrie = 0 # exemple de valeur pour le champ hygro
    temperature = 0
    humidite = random.randint(0,100)
    capteur = 'capteur_humi' # exemple de valeur pour le champ capteur
    capteur2 = 'capteur_hygro'

    cursor = connection.cursor()

    cursor.execute(mySql_insert, (chrono,hygrometrie,capteur,temperature,humidite))
    connection.commit()
    print(cursor.rowcount, "Record inserted successfully into mesure table")
    cursor.close()

except mysql.connector.Error as error:
    print("Failed to insert record into mesure table {}".format(error))

finally:
    if connection.is_connected():
        connection.close()
        print("MySQL connection is closed")