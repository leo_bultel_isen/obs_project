<?php
include("includes/get_controls_arrosage.php");
include("includes/script_jeunes_plants.php");
?>

<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Contrôle Jeunes Plants</title>
	<script src="js/gauge-min.js"></script>
	<script src="node_modules/chart.js/dist/chart.umd.js"></script>
	<link rel="stylesheet" href="css/style_jeunes_plants.css">
</head>

<body>
	<h1>Contrôle zone jeunes plants</h1>
	<div class="control_panels">
		<div class="panel1">
			<div class="control_container">
				<canvas id="gauge-container-hygro"></canvas>
				<div id="preview-textfield-hygro">0%</div>
				<p>Hygrométrie</p>
			</div>
			<div class="control_container">
				<canvas id="gauge-container-temp"></canvas>
				<div id="preview-textfield-temp">0%</div>
				<p>Température</p>
			</div>
		</div>
		<div class="historic">
			<h2 id="historic_title"></h2>
			<canvas id="graphique"></canvas>
		</div>
		<div class="panel2">
			<a href="accueil.php">
				<button type="button" class="btn blue">
					<p class="blue">Accueil</p>
				</button>
			</a>
			<form method="post">
				<input type="hidden" name="submit" value="true">
				<button type="submit" class="btn green">
					<p class="green">Rafraichir</p>
				</button>
			</form>
			<div class="panneau_controle_arrosage">
				<h2>Contrôle Arrosage</h2>
				<iframe name="frame"></iframe>
				<form class="control_form" action="includes/send_controls_arrosage.php" method="post" target="frame">

					<label id="label-text">Seuil d'humidité pour arrosage : </label>
					<input type="range" id="taux_humi_arrosage" name="taux_humi_arrosage" min="0" max="100" value="<?php echo $seuil; ?>" step="1">
					<div id="pourcent_display"><?php echo $seuil; ?> %</div>


					<label id="label-text">Durée d'arrosage : </label>
					<input type="range" id="duree_arrosage" name="duree_arrosage" min="0" max="10" step="0.5" value="<?php echo $duree; ?>">
					<span id="secondes_display"><?php echo $duree; ?> secondes</span>

					<br>

					<input type="submit" id="envoi_instructions" value="Envoyer les instructions" name="envoi_instructions" />
				</form>
			</div>
		</div>
		<div class="panel3">
			<div class="selection_points">
				<form action="jeunes_plants.php" method="get">
					<label for="nb_points">Selection du nombre de données : </label>
					<input type="number" id="nb_points" name="nb_points" min="5" max="1000" value="10">
					<input type="submit" value="Valider" name="ok" />
				</form>
				<form action="post">
					<span>Exporter les données :<input type="submit" value="Valider" name="export" /></span>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var data_hygro = <?php echo json_encode($data_hygro) ?>;
		var data_temp = <?php echo json_encode($data_temp) ?>;
		var data = <?php echo json_encode($data) ?>;
		var time = <?php echo json_encode($time) ?>;
		var nb_points = <?php echo $nb_points; ?>;
	</script>
	<script type="text/javascript" src="js/jeunes_plants.js"></script>
</body>

</html>