<?php
include("includes/script_production.php");
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Contrôle Jeunes Plants</title>
	<script src="js/gauge-min.js"></script>
	<script src="node_modules/chart.js/dist/chart.umd.js"></script>
	<link rel="stylesheet" href="css/style_production.css">
</head>
<body>
    <h1>Contrôle zone production</h1>
	<div class="control_panels">
		<div class="panel1">
			<div class="control_container">
				<canvas id="gauge-container-humi"></canvas>
				<div id="preview-textfield-humi">0%</div>
				<p>Humidité du sol</p>
			</div>
		</div>
		<div class="historic">
			<h2 id="historic_title"></h2>
			<canvas id="graphique"></canvas>
		</div>
		<div class="panel2">
			<a href="accueil.php">
				<button type="button" class="btn blue"><p class="blue">Accueil</p></button>
			</a>
			<form method="post">
				<input type="hidden" name="submit" value="true">
				<button type="submit" class="btn green"><p class="green">Rafraichir</p></button>
			</form>
			<h4 id="arrosage"></h4>
		</div>
		<div class="panel3">
			<div class="selection_points">
				<form  action="production.php" method="get">
				<label for="nb_points">Selection du nombre de données : </label>
				<input type="number" id="nb_points" name="nb_points" min="5" max="1000" value="10">
				<input type="submit" value="Valider" name="ok"/>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
    	var data_humi = <?php echo json_encode($data_humi) ?>;
    	var data = <?php echo json_encode($data) ?>;
    	var time = <?php echo json_encode($time) ?>;
		var nb_points = <?php echo $nb_points; ?>;
	</script>
	<script type="text/javascript" src="js/production.js"></script>
</body>
</html>