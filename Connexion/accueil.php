<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style_accueil.css">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700&display=swap" rel="stylesheet">
   

</head>

    <body>

    
        <form action="logout.php" method="post">
            <input type="hidden" name="logout" value="true">
            <button type="submit" class="btn" >        
                    <p>Déconnexion</p>
            </button>
        </form>
        
            
        <div class="colonne_gauche">        <div class="texte_gauche">
            <h1>PROJET A</h1>
            <p>Zone de Production</p>
            </div>
            <div class="project_button_div">
                <a href="production.php" class="project_btn">
                    <span class="project_btn__circle"></span>
                    <span class="project_btn__white-circle">
                    <svg xmlns="http://www.w3.org/2000/svg" id="icon-arrow-right" viewBox="0 0 21 12">
                        <path d="M17.104 5.072l-4.138-4.014L14.056 0l6 5.82-6 5.82-1.09-1.057 4.138-4.014H0V5.072h17.104z"></path>
                    </svg>
                    </span>
                    <span class="project_btn__text">Sélectionner ce projet</span>
                </a>
            </div>
            <div class="project_image"><img src="images/serre.jpg"></div>
        </div>
        <div class="colonne_droite">
            <div class="texte_droite">
                <h1>PROJET B</h1>
                <p>Zone Jeunes cultures</p>
            </div>
            <div class="project_button_div">
                <a href="jeunes_plants.php" class="project_btn">
                    <span class="project_btn__circle"></span>
                    <span class="project_btn__white-circle">
                    <svg xmlns="http://www.w3.org/2000/svg" id="icon-arrow-right" viewBox="0 0 21 12">
                        <path d="M17.104 5.072l-4.138-4.014L14.056 0l6 5.82-6 5.82-1.09-1.057 4.138-4.014H0V5.072h17.104z"></path>
                    </svg>
                    </span>
                    <span class="project_btn__text">Sélectionner ce projet</span>
                </a>
            </div>
            <div class="project_image"><img src="images/plant.jpg"></div>
        </div>
    </body>
</html>