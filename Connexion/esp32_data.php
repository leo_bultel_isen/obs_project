<?php

include 'includes/database.php';

date_default_timezone_set('Europe/Paris'); 


if ($_SERVER['REQUEST_METHOD'] == "POST") {
    
    $content = trim(file_get_contents("php://input"));
    $decoded = json_decode($content, true);

    if (array_key_exists('humidite', $decoded)) {
        $humidite = $decoded["humidite"];
        $heure_mesure = date("Y-m-d H:i:s"); 
        $capteur = 'capteur_humi';

        //echo "L'humidité reçue est : " . $humidite;
        // Requête SQL pour insérer les données dans la table "mesure"
        $sql = "INSERT INTO mesure (`heure_mesure`,`capteur`, `humidite`) VALUES ('$heure_mesure','$capteur','$humidite')";
        // utiliser la fonction exec() car aucun résultat n'est renvoyé
        $conn->exec($sql);
        $conn = null;
    }
    else if (array_key_exists('hygrometrie', $decoded)) {
        $temperature = $decoded["temperature"];
        $hygrometrie = $decoded["hygrometrie"];
        $heure_mesure = date("Y-m-d H:i:s"); 
        $capteur = 'capteur_hygro';

        //echo "L'humidité reçue est : " . $humidite;

        // Requête SQL pour insérer les données dans la table "mesure"
        $sql = "INSERT INTO mesure (`heure_mesure`,`capteur`,`temperature`, `hygrometrie`) VALUES ('$heure_mesure','$capteur','$temperature','$hygrometrie')";
        // utiliser la fonction exec() car aucun résultat n'est renvoyé
        $conn->exec($sql);
       
        $conn = null;
    } else {
        echo "Aucune donnée d'hygrometrie n'a été reçue.";
    }
} else {
    echo "Le format du contenu doit être JSON.";
}



