<?php

// Connexion à la base de données MySQL
$servername = "localhost";
$username = "phpmyadmin";
$password = "lQYOpZcXFVUHQpPhNrSZfoAQqdNdyIZj
";
$dbname = "obs_mesure";

$conn = new mysqli($servername, $username, $password, $dbname);

// Vérification de la connexion
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Requête SQL pour récupérer les données de la table
$sql = "SELECT mesure FROM obs_mesure";

$result = $conn->query($sql);

// Création du fichier CSV
$filename = "export.csv";
$file = fopen($filename, 'w');

// Écriture de la première ligne avec les noms de colonnes
$header = array("mesure");
fputcsv($file, $header);

// Écriture des données de la table
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $data = array($row["mesure"]);
        fputcsv($file, $data);
    }
}

fclose($file);

echo "Export terminé !";

$conn->close();
