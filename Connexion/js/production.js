// Affichage de la jauge de pourcentage avec la donnée numérique récupérée
var opts_humi = {
    angle: -0.1, // The span of the gauge arc
    lineWidth: 0.25, // The line thickness
    radiusScale: 1, // Relative radius
    percentColors: [[0.0, "#ff0000" ], [0.75, "#6ad700"], [1.0, "#1bea00"]],
    pointer: {
        length: 0.51, // // Relative to gauge radius
        strokeWidth: 0.071, // The thickness
        color: '#ffffff' // Fill color
    },
    limitMax: false,     // If false, max value increases automatically if value > maxValue
    limitMin: false,     // If true, the min value of the gauge will be fixed
    strokeColor: '#c7c7c7',  // to see which ones work best for you
    generateGradient: true,
    highDpiSupport: true,     // High resolution support
    renderTicks: {
        divisions: 10,
        divWidth: 1.6,
        divLength: 0.65,
        divColor: '#333333',
        subDivisions: 2,
        subLength: 0.29,
        subWidth: 0.7,
        subColor: '#666666'
    },
    staticLabels: {
        font: "1rem Poppins",  // Specifies font
        labels: [0,20,40,60,80,100],  // Print labels at these values
        color: "#fffff",  // Optional: Label text color
        fractionDigits: 0  // Optional: Numerical precision. 0=round off.
    }			
};
var target = document.getElementById('gauge-container-humi'); // your canvas element
var gauge = new Gauge(target).setOptions(opts_humi); // create sexy gauge!
gauge.maxValue = 100; // set max gauge value
gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
gauge.animationSpeed = 32; // set animation speed (32 is default value)
gauge.set(data_humi); // set actual value
let textRenderer = new TextRenderer(document.getElementById("preview-textfield-humi"))
textRenderer.render = function(gauge) {this.el.innerHTML = gauge.displayedValue.toFixed(1) + "%"};
gauge.setTextField(textRenderer);

// GRAPHIQUE 
// Création du graphique avec Chart.js
var ctx = document.getElementById('graphique');
  var chart_data = {
    type: 'line',
    datasets: [{
        label: 'Humidité',
        data: data,
        fill: false,
        borderColor: '#ffffff',
        pointRadius: 3,
        pointBackgroundColor: "#979797",
        pointBorderColor: "#ffffff",
        pointBorderWidth: 2,
        pointHoverBackgroundColor: "#ffffff",
        pointHoverBorderColor: "#ffffff"
    }]
};

var chart_options = {
    plugins: {
        legend: {
            display: false
        },
    },
    scales: {
        x: {
            type: 'category',
            labels: time,
            ticks: {
                color: "#ffffff",
                beginAtZero: false,
                padding: 10
            },
            grid: {
                display: true,
                drawOnChartArea: false,
                offset: true,
                color: "#ffffff",
                zeroLineColor: "#ffffff"
            },
        },
        y: {
            title: {
                display: true,
                text: 'Taux humidité sol (%)',
                color: '#ffffff'
            },
            ticks: {
                color: "#ffffff",
                beginAtZero: true
            },
            grid: {
                display: true,
                drawOnChartArea: true,
                color: "#ffffff",
                zeroLineColor: "#ffffff"
            }
        }
    },
    animation: {
        duration: 2000
    }
};

var myChart = new Chart(ctx, {
    type: 'line',
    data: chart_data,
    options: chart_options,
});

var arrosage = document.getElementById("arrosage"); 

if(data_humi > 50){
    arrosage.style["color"]= "#1ea1d5"; 
    arrosage.innerHTML = "ARROSAGE EFFECTUÉ"
}
else{
    arrosage.style["color"]= "#d50808"; 
    arrosage.innerHTML = "ARROSAGE NÉCESSAIRE"
}

var historic = document.getElementById("historic_title"); 
historic.innerHTML = "Historique taux d'humidité • "+ nb_points + " données";