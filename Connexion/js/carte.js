// On initialise la latitude et la longitude de Paris (centre de la carte)
var lat_kernonen = 48.6430034;
var lon_kernonen = -4.0621478;
var lat_keramprat = 48.6846616;
var lon_keramprat = -4.0145739;

var macarte = null;
// Fonction d'initialisation de la carte
function initMap() {
    var capteur_kernonen = "Capteur hygrométrique : Kernonen";
    var capteur_keramprat = "Capteur hygrométrique : Keramprat";

    // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
    macarte = L.map('map').setView([lat_kernonen, lon_kernonen], 11);


    var marker_kernonen = L.marker([lat_kernonen, lon_kernonen]).addTo(macarte);
    var marker_keramprat = L.marker([lat_keramprat, lon_keramprat]).addTo(macarte);

    marker_keramprat.bindPopup(capteur_keramprat).closePopup().on('popupopen', function () {
        this._popup._container.style.backgroundColor = 'red';
        this._popup._content.style.color = 'red';
    });;

    marker_keramprat.on('mouseover', function () {
        this.openPopup();
    });


    marker_keramprat.on('mouseout', function () {
        this.closePopup();
    });


    marker_kernonen.bindPopup(capteur_keramprat).closePopup().on('popupopen', function () {
        this._popup._container.style.backgroundColor = 'red';
        this._popup._content.style.color = 'red';
    });;

    marker_kernonen.on('mouseover', function () {
        this.openPopup();
    });


    marker_kernonen.on('mouseout', function () {
        this.closePopup();
    });



    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
    }).addTo(macarte);

    marker_kernonen.on('click', function () {
        // Rediriger l'utilisateur vers la page souhaitée
        window.location.href = "../Connexion/production.php";
    });

}
window.onload = function () {
    // Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
    initMap();
};