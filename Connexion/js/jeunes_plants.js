// Affichage de la jauge de pourcentage avec la donnée numérique récupérée
var opts_hygro = {
    angle: -0.1, // The span of the gauge arc
    lineWidth: 0.25, // The line thickness
    radiusScale: 1, // Relative radius
    percentColors: [[0.0, "#ff0000" ], [0.75, "#6ad700"], [1.0, "#1bea00"]],
    pointer: {
        length: 0.51, // // Relative to gauge radius
        strokeWidth: 0.071, // The thickness
        color: '#ffffff' // Fill color
    },
    limitMax: false,     // If false, max value increases automatically if value > maxValue
    limitMin: false,     // If true, the min value of the gauge will be fixed
    strokeColor: '#c7c7c7',  // to see which ones work best for you
    generateGradient: true,
    highDpiSupport: true,     // High resolution support
    renderTicks: {
        divisions: 10,
        divWidth: 1.6,
        divLength: 0.65,
        divColor: '#333333',
        subDivisions: 2,
        subLength: 0.29,
        subWidth: 0.7,
        subColor: '#666666'
    },
    staticLabels: {
        font: "1rem Poppins",  // Specifies font
        labels: [0,20,40,60,80,100],  // Print labels at these values
        color: "#fffff",  // Optional: Label text color
        fractionDigits: 0  // Optional: Numerical precision. 0=round off.
    }			
};
var target = document.getElementById('gauge-container-hygro'); // your canvas element
var gauge = new Gauge(target).setOptions(opts_hygro); // create sexy gauge!
gauge.maxValue = 100; // set max gauge value
gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
gauge.animationSpeed = 32; // set animation speed (32 is default value)
gauge.set(data_hygro); // set actual value
let textRenderer = new TextRenderer(document.getElementById("preview-textfield-hygro"))
textRenderer.render = function(gauge) {this.el.innerHTML = gauge.displayedValue.toFixed(1) + "%"};
gauge.setTextField(textRenderer);


var opts_temp = {
    angle: 0.35, // The span of the gauge arc
    lineWidth: 0.1, // The line thickness
    radiusScale: 1, // Relative radius
    limitMax: false,     // If false, max value increases automatically if value > maxValue
    limitMin: false,     // If true, the min value of the gauge will be fixed
    colorStart: '#004462',   // Colors
    colorStop: '#00a3ea',    // just experiment with them
    strokeColor: '#FFFFFF',  // to see which ones work best for you
    generateGradient: true,
    highDpiSupport: true,     // High resolution support
};
var target_temp = document.getElementById('gauge-container-temp'); // your canvas element
var gauge_temp = new Donut(target_temp).setOptions(opts_temp); // create sexy gauge!
gauge_temp.maxValue = 50; // set max gauge value
gauge_temp.setMinValue(-10);  // Prefer setter over gauge.minValue = -10
gauge_temp.animationSpeed = 32; // set animation speed (32 is default value)
gauge_temp.set(data_temp); // set actual value
let textRenderer2 = new TextRenderer(document.getElementById("preview-textfield-temp"))
textRenderer2.render = function(gauge_temp) {this.el.innerHTML = gauge_temp.displayedValue.toFixed(1) + "°C"};
gauge_temp.setTextField(textRenderer2);



// GRAPHIQUE 
// Création du graphique avec Chart.js
var ctx = document.getElementById('graphique');
  var chart_data = {
    type: 'line',
    datasets: [{
        label: 'Hygrométrie',
        data: data,
        fill: false,
        borderColor: '#ffffff',
        pointRadius: 3,
        pointBackgroundColor: "#979797",
        pointBorderColor: "#ffffff",
        pointBorderWidth: 2,
        pointHoverBackgroundColor: "#ffffff",
        pointHoverBorderColor: "#ffffff"
    }]
};

var chart_options = {
    plugins: {
        legend: {
            display: false
        },
    },
    scales: {
        x: {
            type: 'category',
            labels: time,
            ticks: {
                color: "#ffffff",
                beginAtZero: false,
                padding: 10, 
                angle: 20
            },
            grid: {
                display: true,
                drawOnChartArea: false,
                offset: true,
                color: "#ffffff",
                zeroLineColor: "#ffffff"
            },
        },
        y: {
            title: {
                display: true,
                text: 'Taux hygrométrie (%)',
                color: 'white'
            },
            ticks: {
                color: "#ffffff",
                beginAtZero: true
            },
            grid: {
                display: true,
                drawOnChartArea: true,
                color: "#ffffff",
                zeroLineColor: "#ffffff"
            }
        }
    },
    animation: {
        duration: 2000
    }
};

var myChart = new Chart(ctx, {
    type: 'line',
    data: chart_data,
    options: chart_options,
});

var historic = document.getElementById("historic_title"); 
historic.innerHTML = "Historique hygrométrique • "+ nb_points + " données"; 



const valeur_humi = document.getElementById('taux_humi_arrosage');
var rangeValuePourcent = function(){
    var newValue = valeur_humi.value;
    var target = document.getElementById('pourcent_display');
    target.innerHTML = newValue + " %";
}
valeur_humi.addEventListener("input", rangeValuePourcent);


const valeur_temps = document.getElementById('duree_arrosage');
var rangeValueTemps = function(){
    var newValue = valeur_temps.value;
    var target = document.getElementById('secondes_display');
    target.innerHTML = newValue + " secondes";
}
valeur_temps.addEventListener("input", rangeValueTemps);